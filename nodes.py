t = '[ 1 [ 2 3 ] 4 [ 5 [ 6 7 ] ] [ 8 ] ]'.split()
t1= '[ 1 2 3 4 ]'.split()
t2 = '[ 1 2 [ 3 4 ] ]'.split()
t3 = '[ 1 2 [ 3 4 [ 5 6 ] 7 [ 1 2 ] ] ]'.split()
t4 = '[ 1 2 [ 3  [ 4  [ 7 ] ] ] ]'.split()
t5= '[ 1 2 [ 3 4 [ 2 3 ] ] [ 4 6 ] ]'.split()


class Node(object):

    def __init__(self, childList, numberList):
        self.childList = childList
        self.numberList = numberList

    def __repr__(self):
        return 'Node{%s %s}' % (self.numberList,
                                self.childList)

    @classmethod
    def parse_list(cls, elements):
        _, node = Node._parse_list(elements)
        return node

    @classmethod
    def _parse_list(cls, elements):
        numbers = []
        childs = []
        el_len = len(elements)
        simple_childs = 0
        idx = 0
        while idx < el_len - 1:
            ch = elements[idx]
            if ch.isdigit():
                numbers.append(int(ch))
                simple_childs += 1
                idx += 1
            else:
                if idx == 0 and ch == '[':
                    idx += 1
                elif idx != 0 and ch == '[':
                    length, node = Node._parse_list(elements[idx:])
                    simple_childs += length + 2
                    idx += 2 + length
                    childs.append(node)
                elif ch == ']' and idx == el_len - 1:
                    break
                elif ch == ']':
                    break
        return (simple_childs, Node(childs, numbers))

    def sum(self):
        nodes_sum = sum(self.numberList)
        for node in self.childList:
            nodes_sum += node.sum()
        return nodes_sum

if __name__ == "__main__":
    node = Node.parse_list(t)
    print node
    print node.sum()
